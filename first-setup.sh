#!/bin/bash

cd /tmp/

apt-get update
wget https://bitbucket.org/genel/nano-pi/raw/master/package-selections -O /tmp/package-selections
aptitude install $(cat /tmp/package-selections | awk '{print $1}')

php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
php composer-setup.php
mv /tmp/composer.phar /usr/bin/composer

lighttpd-enable-mod fastcgi
lighttpd-enable-mod fastcgi-php

/etc/init.d/lighttpd restart

cd /root/
git clone https://bitbucket.org/genel/nano-pi.git

mkdir -p /root/.ssh/
cp -Rf /root/nano-pi/.ssh/id_rsa /root/.ssh/id_rsa
cp -Rf /root/nano-pi/.ssh/id_rsa.pub /root/.ssh/id_rsa.pub
chmod 600 /root/.ssh/id_rsa
chmod 600 /root/.ssh/id_rsa.pub

cd /root/
git clone git@bitbucket.org:genel/tanita-commands.git

cp -f /root/tanita-commands/config/root/.ssh/id_rsa /root/.ssh/id_rsa
cp -f /root/tanita-commands/config/root/.ssh/id_rsa.pub /root/.ssh/id_rsa.pub
chmod 600 /root/.ssh/id_rsa
chmod 600 /root/.ssh/id_rsa.pub

mkdir -p /var/www/html/
cd /var/www/html/

git clone git@bitbucket.org:genel/tanita-tarti.git
git clone git@bitbucket.org:genel/tanita-wifi.git

cd /var/www/html/tanita-tarti
composer update

chmod -R 777 storage/
chmod -R 777 bootstrap/cache/

